# Test Consul setup

To build, run the script in the parent folder:  [docker.build.bat](..\docker.build.bat) 

Or not build and run the script below directly, and this will pull a prebuilt image from Docker Hub.

Run the script:  [docker-compose.up.bat](docker-compose.up.bat) 