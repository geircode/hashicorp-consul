cd %~dp0
docker rm -f hashicorp-consul-builder-1
docker-compose -f docker-compose.yml down --remove-orphans
docker-compose -f docker-compose.yml up --build --remove-orphans