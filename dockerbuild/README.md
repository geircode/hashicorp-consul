# Docker build from Linux

If the Dockerfile depends on shell scripts such as "***docker-entrypoint.sh***", then building the Docker Image from Windows will sometimes change the format of the file.

The solution is to start the build from a Linux container and convert all files back to linux format using "dos2unix". The container builds the image against the Docker Host/daemon.