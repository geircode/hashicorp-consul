#!/bin/sh
cd /app
dos2unix *
docker build --no-cache -f Dockerfile -t geircode/hashicorp-consul:latest .
