# HashiCorp's Consul without exposed volumes

https://github.com/hashicorp/consul
https://hub.docker.com/_/consul

## Orignal Dockerfile
https://github.com/hashicorp/docker-consul

## Sandbox Container
The Dockerfile is without the VOLUME /data/files to make it possible to use "docker commit" and reuse the Docker Image when testing and developing.
Build the Docker Image on Windows using the script located at: "dockerbuild\docker-compose.up.bat"

## Building the image
Run  [docker.build.bat](docker.build.bat) 

Sometimes, for some reason, the keyserver is not responding and creating the image will fail. The resolve is to try again until it works...

## Access the Consul instance

[http://localhost:8500/ui/dc1/services](http://localhost:8500/ui/dc1/services)

